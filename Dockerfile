FROM node:12
WORKDIR /app

COPY . .
RUN npm i --production

CMD npm start